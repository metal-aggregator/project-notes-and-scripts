# Project components
## Lambdas
To create or update a particular Lambda function, run the`CICD/update.bat` script in the associated Lambda's code base. 

The scripts make the following assumptions:
* Absolute location of code base
* Name of S3 bucket to target for the SAM build/upload
* IAM role assigned to each Lambda's template.yml is hard coded
* Environment variables in template.yml contain hard coded DNS names
* Security group IDs in template.yml are hard coded
* Subnet IDs in template.yml are hard coded.

## Microservices
To create a new docker image from the Spring boot app and upload it to ECR, manually run the steps in `ecs/ecs notes.txt` in this repository.

Task definitions and services are currently manually configured.

To start the ECS services (scale them to 1 instance), run `ecs/start-services.bat` in this repository. 
To stop the ECS services (scale them to 0 instances), run `ecs/stop-services.bat` in this repository.

Note to self: Determine whether this can happen before/after starting up the ALBs/DNS

The scripts make the following assumptions:
* Hard coded ECR URL
* Manual process per service to get this set up
* Need to root around the ECS web console to get the new task definitions set up and applied to the services

## Microservice load balancers and DNS
To create the ALBs and register internal DNS to route to them, run `cloudformation/create-root.bat`. This will create the 
ALB, register it to the microservice-specific target groups, and create static DNS from the ALB's DNS name. 

To tear down the ALBs, run `cloudformation/delete-root.bat`

The scripts make the following assumptions:
* Hard coded S3 URL
* Hard coded security group ID
* Hard coded subnet IDs
* Hard coded target group ARN
* Hard coded DNS name

## Step functions
The step function source code is in `stepfunction/amg-scraper.json`. This currently needs to be manually applied via the AWS web console to
update the step function. 

Assumptions: 
* Hard coded lambda names

## DynamoDB
To create the tables and triggers, run `db-schema/remote/cloudformation/create-database.bat`.

Currently not an elegant way to update the existing stack without wiping the tables altogether.

## Frontend app
TBD