To create an ECS service, the load balancer must exist if you're trying to specify a target group.
This is a bit weird, since you can just delete the load balancer and the service will still be fine.
So, to minimize cost, we need to *create* the service with the load balancer. However, afterwards, we 
need to delete the load balancer, and update the service when scaling the desired instance count.