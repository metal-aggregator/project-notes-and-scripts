aws dynamodb create-table ^
--table-name Reviews ^
--attribute-definitions AttributeName=id,AttributeType=S AttributeName=url,AttributeType=S ^
--key-schema AttributeName=id,KeyType=HASH ^
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 ^
--global-secondary-indexes IndexName=Reviews_url_idx,KeySchema=[{AttributeName=url,KeyType=HASH}],Projection={ProjectionType=ALL},ProvisionedThroughput={ReadCapacityUnits=5,WriteCapacityUnits=5} ^
--endpoint-url http://localhost:8000