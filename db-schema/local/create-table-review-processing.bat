aws dynamodb create-table ^
--table-name Review_Processing ^
--attribute-definitions AttributeName=reviewId,AttributeType=S ^
--key-schema AttributeName=reviewId,KeyType=HASH ^
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 ^
--endpoint-url http://localhost:8000