aws dynamodb create-table ^
--table-name Tags ^
--attribute-definitions AttributeName=tag_name,AttributeType=S ^
--key-schema AttributeName=tag_name,KeyType=HASH ^
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 ^
--endpoint-url http://localhost:8000